import config
import logger
import cmdserver

import bge
import time
import socket
import json
import math
from contextlib import closing

def merge(org, new):
    for key, val in new.items():
        if isinstance(val, dict):
            merge(org[key], val)
        else:
            org[key] = val
    return org

def receive_status():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('127.0.0.1' , 3939))
        s.settimeout(0.1)
        data = ''
        while True:
            res = s.recv(1024)
            if len(res) == 0:
                break
            data += res.decode("UTF-8")
        update_status(data)
        s.shutdown(socket.SHUT_RDWR)
        s.close()
    except socket.timeout:
        return False
    except ConnectionRefusedError:
        return False

# sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# sock.bind(('0.0.0.0', 3940))
# sock.setblocking(0)
# def receive_udp_status():
#     global sock
#     try:
#         data, addr = None, None
#         try:
#             data, addr = sock.recvfrom(1024)
#         except BlockingIOError:
#             return
#         logger.log(data)
#         if not data:
#             return
#     except socket.timeout:
#         return
#     try:
#         req = data.decode()
#         update_status(req)
#     except json.decoder.JSONDecodeError:
#             print("JSON Decode error.")

def update_status(data):
    if not data:
        return
    try:
        cmd = json.loads(data)
        logger.log("[GET]: {}".format(cmd))
        for name, data in cmd.items():
            if name == 'CAMERA':
                merge(config.CAMERA, data)
                config.CAMERA['update'] = True
                continue
            if 'TEXTURES' in data:
                merge(config.OBJECTS[name]['STATUSES']['TEXTURES'], data['TEXTURES'])
                config.OBJECTS[name]['UPDATES']['TEXTURES'] = True
            if 'ARMATURES' in data:
                merge(config.OBJECTS[name]['STATUSES']['ARMATURES'], data['ARMATURES'])
                config.OBJECTS[name]['UPDATES']['ARMATURES'] = True
        print(name)
        print(config.OBJECTS['mizuki']['STATUSES'])
    except json.decoder.JSONDecodeError:
        logger.log("[JSDecErr]: {}".format(data))

def init_server():
    cmdserver.init()
    logger.log("INIT UDP SERVER!")

def init_service():
    config.GAME_OBJ = {}
    for scene in bge.logic.getSceneList():
        for obj in scene.objects:
            config.GAME_OBJ[obj.name] = obj
    for cdata in config.OBJECTS.values():
        cdata['TEXTURES_OBJ'] = {}
        for oname, odata in cdata['TEXTURES'].items():
            cdata['TEXTURES_OBJ'][oname] = {}
            for tkey, tname in odata.items():
                if tkey == 'DEFAULT':
                    continue
                cdata['TEXTURES_OBJ'][oname][tkey] = bge.texture.ImageFFmpeg(bge.logic.expandPath(tname))

def update_objects():
    print(config.OBJECTS['mizuki']['STATUSES'])
    for cdata in config.OBJECTS.values():
        if cdata['UPDATES']['ARMATURES']:
            update_armatures(cdata)
        if cdata['UPDATES']['TEXTURES']:
            update_textures(cdata)

def update_textures(cdata):
    for oname, tname in cdata['STATUSES']['TEXTURES'].items():
        obj = config.GAME_OBJ[oname]
        matID = bge.texture.materialID(obj, cdata['TEXTURES'][oname]['DEFAULT'])
        bge.logic.texture = bge.texture.Texture(obj, matID)
        bge.logic.texture.source = cdata['TEXTURES_OBJ'][oname][tname]
        bge.logic.texture.refresh(False)
    cdata['UPDATES']['TEXTURES'] = False

def update_armatures(cdata):
    for aname, adata in cdata['STATUSES']['ARMATURES'].items():
        obj = config.GAME_OBJ[aname]
        for chan in obj.channels:
            chan.rotation_mode = bge.logic.ROT_MODE_XYZ
        for bname, bval in adata.items():
            obj.channels[bname].joint_rotation = (bval[0], bval[1], bval[2])
            logger.log("[{}]: {}".format(bname, (bval[0], bval[1], bval[2])))
        obj.update()
    cdata['UPDATES']['ARMATURES'] = False


# def update_camera(radius, theta, phi):
#     scene = bge.logic.getCurrentScene()
#     obj = scene.objects['Camera']
#     pos = convert(radius, theta, phi)
#     obj.position.x = pos[1]
#     obj.position.y = pos[0]
#     obj.position.z = pos[2]
#     obj.worldOrientation = (-math.radians(theta), 0, -math.radians(phi))

# def rotate():
#     if config.MODELS[0]['update'] == False:
#         return
#     config.MODELS[0]['update'] = False

#     x, y, z = config.MODELS[0]['x'], config.MODELS[0]['y'], config.MODELS[0]['z']
#     #cont = bge.logic.getCurrentController()
#     #obj = cont.owner
#     obj = g_objects["Armature"]
#     for chan in obj.channels:
#         chan.rotation_mode = 1

#     # obj.channels["Master"].joint_rotation = (0, 0, y/15)
#     for name, func in config.ARMATURES[0].items():
#         obj.channels[name].joint_rotation = func(x,y,z)
#     obj.update()

# def yes():
#     cont = bge.logic.getCurrentController()
#     act = cont.getActuator("Action")
#     bge.logic.addActiveActuator(act, 1)

# def joystick():
#     # print(bge.logic.joysticks)
#     joystick = bge.logic.joysticks[0]
#     # print(joystick.axisValues)
#     # [0.07269508957182531, -0.050567626953125, -1.0, -0.01251220703125, 0.016266365550706503, -1.0]
#     axis = joystick.axisValues
#     logger.log("[Motion]LS: LR - {0:+0.2f}, UD: - {1:+0.2f}".format(axis[0], axis[1]))
#     logger.log("[Motion]RS: LR - {0:+0.2f}, UD: - {1:+0.2f}".format(axis[3], axis[4]))
#     logger.log("[Motion]TR: LT - {0:+0.2f}, RT: - {1:+0.2f}".format(axis[2], axis[5]))
#     config.MODELS[0]['x'] = axis[1] * config.MODELS[0]['d']
#     config.MODELS[0]['y'] = axis[0] * config.MODELS[0]['d']
#     config.MODELS[0]['update'] = True

# def interval_x():
#     config.MODELS[0]['ivx'] += 1
#     config.MODELS[0]['x'] += math.sin(config.MODELS[0]['ivx']) / 10
#     config.MODELS[0]['update'] = True

# def interval_y():
#     config.MODELS[0]['ivy'] += 1
#     config.MODELS[0]['y'] += math.sin(config.MODELS[0]['ivy']) / 20
#     config.MODELS[0]['update'] = True

#     # 変更対象オブジェクトのコントローラで
#     # 呼び出す必要あり
#     # TODO: 呼び出し回数を減らす
# def change_texture():
#     #cont = bge.logic.getCurrentController()
#     #obj = cont.owner
#     obj = get_object("body")
#     matID = bge.texture.materialID(obj, 'IMbody.png')
#     bge.logic.texture = bge.texture.Texture(obj, matID)
#     bge.logic.texture.source = config.TEXTURES[0][config.MODELS[0]['face']]
#     bge.logic.texture.refresh(False)


# def show_obj_list():
#     scenes = bge.logic.getSceneList()
#     print(scenes)
#     for scene in scenes :
#         print("scene : {}({})".format(scene.name, type(scene)))
#         for obj in scene.objects :
#             print("   object : {}({})".format(obj.name, type(obj)))
#             for sensor in obj.sensors :
#                 print("      sensor : {}({})".format(sensor.name, type(sensor)))
#             for cont in obj.controllers :
#                 print("      controller : {}({})".format(cont.name, type(cont)))
#             for actu in obj.actuators :
#                 print("      actuator : {}({})".format(actu.name, type(actu)))

# scene : Scene(<class 'KX_Scene'>)
#    object : Armature(<class 'BL_ArmatureObject'>)
#       sensor : Joystick(<class 'SCA_JoystickSensor'>)
#       sensor : Interval_X(<class 'SCA_DelaySensor'>)
#       sensor : Interval_Y(<class 'SCA_DelaySensor'>)
#       sensor : Yes(<class 'SCA_KeyboardSensor'>)
#       sensor : Always(<class 'SCA_AlwaysSensor'>)
#       sensor : Delay(<class 'SCA_DelaySensor'>)
#       controller : Python(<class 'SCA_PythonController'>)
#       controller : Python.001(<class 'SCA_PythonController'>)
#       controller : Python.002(<class 'SCA_PythonController'>)
#       controller : And(<class 'SCA_ANDController'>)
#       controller : Python.003(<class 'SCA_PythonController'>)
#       controller : Python.004(<class 'SCA_PythonController'>)
#       actuator : Action(<class 'BL_ActionActuator'>)
#    object : skirt(<class 'KX_GameObject'>)
#    object : nitcap(<class 'KX_GameObject'>)
#    object : body(<class 'KX_GameObject'>)
#       controller : Python(<class 'SCA_PythonController'>)
#       actuator : Action(<class 'BL_ActionActuator'>)
#    object : socks(<class 'KX_GameObject'>)
#    object : shoes(<class 'KX_GameObject'>)
#    object : ribbon(<class 'KX_GameObject'>)
#    object : tops(<class 'KX_GameObject'>)
#    object : inner(<class 'KX_GameObject'>)
#    object : shorts(<class 'KX_GameObject'>)
#    object : hair(<class 'KX_GameObject'>)
#    object : CLamp-Front(<class 'KX_GameObject'>)
#    object : Lamp-right(<class 'KX_LightObject'>)
#    object : Lamp-left(<class 'KX_LightObject'>)
#    object : Lamp(<class 'KX_LightObject'>)
#    object : Camera(<class 'KX_Camera'>)
#       sensor : Joystick(<class 'SCA_JoystickSensor'>)
#       sensor : Joystick.001(<class 'SCA_JoystickSensor'>)
#       sensor : Joystick.002(<class 'SCA_JoystickSensor'>)
#       sensor : Delay(<class 'SCA_DelaySensor'>)
#       controller : Python(<class 'SCA_PythonController'>)
#       controller : Python.001(<class 'SCA_PythonController'>)
#       controller : Python.002(<class 'SCA_PythonController'>)
#       controller : Python.003(<class 'SCA_PythonController'>)
#    object : CLamp-Left(<class 'KX_GameObject'>)
#    object : CLamp-Right(<class 'KX_GameObject'>)
