@echo off

C:\gstreamer\1.0\x86_64\bin\gst-launch-1.0 ^
  ksvideosrc device-index=0 ^
  ! "image/jpeg,width=320,height=240,framerate=30/1" ^
  ! videorate ! "image/jpeg,framerate=30/1" ^
  ! multifilesink max-files=5 location="webcam-%%06d.jpg"
