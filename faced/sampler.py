#-*- coding: utf-8 -*-

import sys

class Sampler:
    def __init__(self, maxlen=10):
        self.samples = list()
        self.maxlen  = maxlen
        self.max      = -sys.maxsize
        self.min      =  sys.maxsize
        self.hist_max = -sys.maxsize
        self.hist_min =  sys.maxsize

    def append(self, n):
        if n > self.hist_max:
            self.hist_max = n
        if n < self.hist_min:
            self.hist_min = 0
        self.samples.append(n)
        if len(self.samples) > self.maxlen:
            self.samples.pop(0)
        self.min = min(self.samples)
        self.max = max(self.samples)
