#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys
import os
import time
import socket
import json
import random
from pathlib import Path
import dlib
from sampler import Sampler
from svgwriter import SvgWriter

IMG_DIR     = '.'
IMG_NAME    = 'sample.jpg'
SVG_NAME    = 'sample.svg'
LEARN_DATA  = 'learn.dat'
WIDTH       = 320
HEIGHT      = 240
SERVER_IP   = '127.0.0.1'
SERVER_PORT = 3939
SVGOPT      = "stroke=\"red\" stroke-width=\"3\""

detector    = dlib.get_frontal_face_detector()
predictor   = dlib.shape_predictor(LEARN_DATA)
sock        = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mouse_sampler = Sampler()

def print_face_pos(shape):
    print("右目: {} - {}".format(shape.part(36),
                                 shape.part(39)))
    print("左目: {} - {}".format(shape.part(42),
                                 shape.part(45)))
    print("鼻: {} - {}".format(shape.part(30),
                               shape.part(33)))
    print("口(上下): {} - {}".format(shape.part(51),
                                     shape.part(57)))
    print("口(左右): {} - {}".format(shape.part(48),
                                     shape.part(54)))
    print("口(開き): {}".format(shape.part(57).y - shape.part(51).y))
    print("右目(開き): {}".format(shape.part(39).y - shape.part(36).y))
    print("左目(開き): {}".format(shape.part(45).y - shape.part(42).y))

def detect_face():
    return dets

def image_processor():
    samples = list(Path('.').glob('webcam-*.jpg'))
    if len(samples) == 0:
        return None
    sorted(samples)[-1].replace(IMG_NAME)
    img = dlib.load_rgb_image(IMG_NAME)
    dets = detector(img, 1)
    print("Number of faces detected: {}".format(len(dets)))
    svg = SvgWriter(SVG_NAME, WIDTH, HEIGHT, SVGOPT)
    c, x, y = 0, 0, 0
    for k, d in enumerate(dets):
        print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            k, d.left(), d.top(), d.right(), d.bottom()))
        shape = predictor(img, d)
        print_face_pos(shape)
        svg.update(d, shape)
        c += 1
        x += shape.part(30).x
        y += shape.part(30).y
        m  = shape.part(57).y - shape.part(51).y
        mouse_sampler.append(m)
    svg.close()

    if (c > 0):
        xabs, yabs = WIDTH / 2, HEIGHT / 2
        xpos, ypos = (x/c - xabs) / xabs, (y/c - yabs) / yabs
        if m > (mouse_sampler.hist_max - mouse_sampler.hist_min) / 2 + mouse_sampler.hist_min:
            mouse = 'om'
        else:
            mouse = 'cm'

        if random.randrange(100) > 90:
            eye = 'ce'
        else:
            eye = 'oe'
            print({'x': xpos * -1, 'y': ypos, 'z': 0, 'mouse': mouse, 'eye': eye})
        return {'x': xpos * -1, 'y': ypos, 'z': 0, 'mouse': mouse, 'eye': eye}
    else:
        return None

if __name__ == "__main__":
    while True:
        res = image_processor()
        if not res:
            time.sleep(0.01)
            continue
        js  = json.dumps(res)
        print(js)
        sock.sendto(js.encode("UTF-8"), (SERVER_IP, SERVER_PORT))
#     time.sleep(0.01)
