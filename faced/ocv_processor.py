#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os
import math
import time
import socket
import json
import random
import cv2
import dlib
from svgwriter import SvgWriter

IMG_DIR     = '.'
IMG_NAME    = 'sample.jpg'
SVG_NAME    = 'sample.svg'
VDEV_ID     = 0
LEARN_DATA  = ['/usr/share/dlib/shape_predictor_68_face_landmarks.dat', 'learn.dat']
WIDTH       = 320
HEIGHT      = 240
SERVER_IP   = '127.0.0.1'
SERVER_PORT = 3939
SVGOPT      = "stroke=\"red\" stroke-width=\"3\""
count       = 0

sock        = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

video       = cv2.VideoCapture(VDEV_ID)
video.set(cv2.CAP_PROP_FRAME_WIDTH,  WIDTH)
video.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)

detector    = dlib.get_frontal_face_detector()
for LDATA in LEARN_DATA:
    if os.path.exists(LDATA):
        predictor   = dlib.shape_predictor(LDATA)
        break

def distance(p1, p2):
    return math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)

def image_processor():
    global count
    frame = video.read()
    if frame[0] != True:
        print("CAMERA ERROR!!!")
        return {'x': 0, 'y': 0, 'z': 0, 'orientation': 0}
    count += 1
    if count % 10 == 0:
        cv2.imwrite('static/sample.jpg', frame[1])
    dets = detector(frame[1], 1)
    svg  = SvgWriter(SVG_NAME, WIDTH, HEIGHT, SVGOPT)
    c, x, y = 0, 0, 0
    left, center, right = None, None, None
    for k, d in enumerate(dets):
        shape = predictor(frame[1], d)
        c += 1
        # 顔の中心(30)
        x += shape.part(30).x
        y += shape.part(30).y
        # 口の左(48), 中央(62), 右(54)
        left   = shape.part(48)
        center = shape.part(62)
        right  = shape.part(54)
        mouse  = distance(shape.part(62), shape.part(66))
        svg.update(d, shape)
    svg.close()

    if (c > 0):
        leftweight  = distance(left, center)
        rightweight = distance(center, right)
        orientation = leftweight - rightweight
        xabs, yabs  = WIDTH / 2, HEIGHT / 2
        xpos, ypos  = (x/c - xabs) / xabs, (y/c - yabs) / yabs
        eye         = random.randrange(100)
        return {'x': xpos * -1, 'y': ypos, 'z': 0, 'mouse': mouse, 'eye': eye}
    else:
        return None

if __name__ == "__main__":
    while True:
        res = image_processor()
        if not res:
            time.sleep(0.01)
            continue
        js  = json.dumps(res)
        print(js)
        sock.sendto(js.encode("UTF-8"), (SERVER_IP, SERVER_PORT))
