#!/usr/bin/python3
#-*- coding: utf-8 -*-

import time
import math
import socketserver
import socket
import json
#import processor
from threading import Thread

#SERVER_IP   = '127.0.0.1'
SERVER_IP   = '0.0.0.0'
SERVER_UDP_PORT = 3939
SERVER_TCP_PORT = 3939
#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

ARMATURE = {
    'Head':    lambda x, y, z: (x/1, y/1, z/1),
    'Body02':  lambda x, y, z: (0, y/15-0.26, y/15),
    'Arm01_L': lambda x, y, z: (x/30, y/30, z/30),
    'Arm01_R': lambda x, y, z: (x/30, y/30, z/30),
    'Arm02_L': lambda x, y, z: (x/4-0.95, y/4-0.08, z/4),
    'Arm02_R': lambda x, y, z: (x/4-0.95, y/4-0.08, z/4),
    'Arm03_L': lambda x, y, z: (x/4+0.18, y/4, z/4),
    'Arm03_R': lambda x, y, z: (x/4+0.09, y/4, z/4+0.34),
    'Hand_L':  lambda x, y, z: (x/4+0.17, y/4, z/4),
    'Hand_R':  lambda x, y, z: (x/4+0.17, y/4, z/4),
    'Master':  lambda x, y, z: (0, 0, y/15)
}

# template = {
#     'CAMERA': {
#         'radius':   10,
#         'theta':    270,
#         'phi':      0,
#         'x_offset': 0,
#         'y_offset': 0,
#         'z_offset': 0
#     },
#     'mizuki': {
#         'TEXTURES': {
#             'body': 'cm_oe'
#         },
#         'ARMATURES': {
#             'Armature': {
#             'Head':    [0,0,0],
#                 'Arm02_L': [0,0,0],
#                 'Arm02_R': [0,0,0],
#                 'Arm03_L': [0,0,0],
#                 'Arm03_R': [0,0,0],
#                 'Master':  [0,0,0]
#             }
#         }
#     }
# }

x, y, z = 0, 0, 0
img_x, img_y, img_z = 0, 0, 0
new_x, new_y, new_z = 0, 0, 0
eye = 'ce'
mouse = 'om'
update_texture = False
mv_speed = 0.2

def move(prv, new):
    if prv < new:
        n = prv + mv_speed
        if n > new:
            n = new
    else:
        n = prv - mv_speed
        if n < new:
            n = new
    return n

def build_msg():
    global x, y, z
    global new_x, new_y, new_z
    dat = {'mizuki': {'ARMATURES': {'Armature': {}}}}
    new_x = move(new_x, img_x)
    new_y = move(new_y, img_y)
    new_z = move(new_z, img_z)
    x = math.sin(time.time() * 0.5) / 100 + new_x
    y = math.sin(time.time() * 2) / 100 + new_y
    z = math.sin(time.time() * 1) / 100 + new_z
    for aname, alambda in ARMATURE.items():
        dat['mizuki']['ARMATURES']['Armature'][aname] = alambda(y, x, z)
    if update_texture:
        dat['mizuki']['TEXTURES'] = {}
        dat['mizuki']['TEXTURES']['body'] = '{}_{}'.format(mouse, eye)
    jsdat = json.dumps(dat)
    return jsdat.encode('utf-8')

class MizukiHandler(socketserver.BaseRequestHandler):
    def handle(self):
        client = self.request
        res = build_msg()
        if res != None:
            client.send(res)

class FacedHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global img_x, img_y, img_z, eye, mouse, update_texture
        req = self.request
        print(req[0])
        dreq = req[0].decode('UTF-8')
        js = json.loads(dreq)
        img_x, img_y, img_z = js['x'], js['y'], js['z']
        if eye != js['eye'] or mouse != js['mouse']:
            update_texture = True
        else:
            update_texture = False
        eye = js['eye']
        mouse = js['mouse']

global udpsv, tcpsv
def udpserver():
    global udpsv
    udpsv = socketserver.ThreadingUDPServer((SERVER_IP, SERVER_UDP_PORT), FacedHandler)
    udpsv.serve_forever()

def tcpserver():
    global tcpsv
    socketserver.ThreadingTCPServer.allow_reuse_address = True
    tcpsv = socketserver.ThreadingTCPServer((SERVER_IP, SERVER_TCP_PORT), MizukiHandler)
    tcpsv.serve_forever()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# def udpclient():
#     global sock
#     while True:
#         msg = build_msg()
#         sock.sendto(msg, ('127.0.0.1', 3940))
#         time.sleep(0.05)
    
if __name__ == "__main__":
    udps = Thread(target=udpserver)
    tcps = Thread(target=tcpserver)
    #udpc = Thread(target=udpclient)
    #udps.daemon = True
    #tcps.daemon = True
    #udpc.daemon = True
    udps.start()
    tcps.start()
    #udpc.start()
    try:
        udps.join()
        tcps.join()
    except KeyboardInterrupt:
        udpsv.shutdown()
        tcpsv.shutdown()
    #udpc.join()
