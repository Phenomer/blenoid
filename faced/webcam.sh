#!/bin/sh

gst-launch-1.0 v4l2src device=/dev/video0 \
	       ! "image/jpeg,width=320,height=240,framerate=30/1" \
	       ! videorate \
	       ! "image/jpeg,framerate=5/1" \
	       ! multifilesink max-files=30 location="./webcam-%06d.jpg"

# Windows
# gst-launch-1.0 ksvideosrc device-index=0 ! "image/jpeg,width=320,height=240,framerate=15/1" ! videorate ! "image/jpeg,framerate=5/1" ! multifilesink max-files=15 location="/sample/webcam-%06d.jpg"
# decodebin?
