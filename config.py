#-*- coding: utf-8 -*-

APPNAME = 'mizuki'
LOGFILE = '/tmp/' + APPNAME + '.log'
SERVER_IP   = "0.0.0.0"
SERVER_PORT = 3939

CAMERA = {
    'radius':   10,  # 3..30
    'theta':    270, # 0..360
    'phi':      0,   # -180..180
    'x_offset': 0,
    'y_offset': 0,
    'z_offset': 0,
    'update':   False
}

OBJECTS = {
    'mizuki': {
        'TEXTURES': {
            'body':{
                'DEFAULT': 'IMbody.png',
                'cm_oe': "//body.png",
                'cm_ce': "//body_close_mouse_close_eye.png",
                'om_oe': "//body_open_mouse_open_eye.png",
                'om_ce': "//body_open_mouse_close_eye.png"
            }
        },
        # ここはmotion.init_serviceで自動生成
        # 'TEXTURES_OBJ':{
        #     'body': {
        #         # 'cm_oe': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body.png"))
        #     }
        # },

        # blender側のX, Y, Zは
        # X: 上下, Y: 左右, Z: 左右傾き
        'ARMATURES': {
            'Armature':{
                'Head':    lambda x,y,z: (x/30, y/30, z/30),
                'Arm02_L': lambda x,y,z: (x/30-0.8, y/30, z/30),
                'Arm02_R': lambda x,y,z: (x/30-0.8, y/30, z/30),
                'Arm03_L': lambda x,y,z: (x/30+0.15, y/30, z/30),
                'Arm03_R': lambda x,y,z: (x/30+0.15, y/30, z/30),
                'Master':  lambda x,y,z: (0, 0, y/15)
            }
        },

        'UPDATES':{
            'TEXTURES': False,
            'ARMATURES': False,
        },

        # 外から更新できるのはここだけ
        'STATUSES':{
            'TEXTURES': {
                'body': 'cm_oe'
            },
            'ARMATURES': {
                'Armature': {
                    'Head':    [0, 0, 0],
                    'Arm02_L': [0, 0, 0],
                    'Arm02_R': [0, 0, 0],
                    'Arm03_L': [0, 0, 0],
                    'Arm03_R': [0, 0, 0],
                    'Master':  [0, 0, 0]
                }
            }
        }
    }
}

# MODELS = [
#     {
#         'x':      0,
#         'y':      0,
#         'z':      0,
#         'd':      5,
#         'rd':     0,
#         'ivx':    0,
#         'ivy':    0,
#         'face':   'cm_oe',
#         'update': False
#     },

#     {
#         'x':      0,
#         'y':      0,
#         'z':      0,
#         'd':      5,
#         'rd':     0,
#         'ivx':    0,
#         'ivy':    0,
#         'face':   'cm_oe',
#         'update': False
#     },
# ]

# TEXTURES = [
#     {
#         'cm_oe': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body.png")),
#         'cm_ce': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_close_mouse_close_eye.png")),
#         'om_oe': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_open_mouse_open_eye.png")),
#         'om_ce': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_open_mouse_close_eye.png"))
#     },
#         {
#         'cm_oe': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body.png")),
#         'cm_ce': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_close_mouse_close_eye.png")),
#         'om_oe': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_open_mouse_open_eye.png")),
#         'om_ce': bge.texture.ImageFFmpeg(bge.logic.expandPath("//body_open_mouse_close_eye.png"))
#     }
# ]

# ARMATURES = [
#     {
#     "Head":    lambda x,y,z: (x/10, y/10, z/10),
#     "Arm02_L": lambda x,y,z: (x/30-0.8, y/30, z/30),
#     "Arm02_R": lambda x,y,z: (x/30-0.8, y/30, z/30),
#     "Arm03_L": lambda x,y,z: (x/30+0.15, y/30, z/30),
#     "Arm03_R": lambda x,y,z: (x/30+0.15, y/30, z/30),
#     "Master":  lambda x,y,z: (0, 0, y/15)
#     },
#     {
#     "Head":    lambda x,y,z: (x/10, y/10, z/10),
#     "Arm02_L": lambda x,y,z: (x/30-0.8, y/30, z/30),
#     "Arm02_R": lambda x,y,z: (x/30-0.8, y/30, z/30),
#     "Arm03_L": lambda x,y,z: (x/30+0.15, y/30, z/30),
#     "Arm03_R": lambda x,y,z: (x/30+0.15, y/30, z/30),
#     "Master":  lambda x,y,z: (0, 0, y/15)
#     }
# ]
