# Blenoid
Blender Game Engineとモーションキャプチャを用いてキャラクターを動かすためのものです。

# 必要なもの
- Blender 2.79以降
- Python3.7以降
- dlib
- それなりに強めのパソコン
  (検証環境: ThinkPad X260 Core i5-6300U, 8GB memory,
   Linux and Windows10 WSL)
- Webカメラ

# Setup
```
	$ sudo apt install libdlib-dev python3-pip python3-numpy libjpeg-dev cmake gstreamer1.0-tools gstreamer1.0-plugins-good
	$ pip3 install --user numpy dlib
```
