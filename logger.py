#-*- coding: utf-8 -*-

import config

import os
import time
import inspect
from contextlib import closing

def log(msg):
    with closing(open(config.LOGFILE, 'a')) as l:
        caller = inspect.stack()[1]
        # os.path.basename(filename), lineno, function
        l.write("[{} {}({}) {}] {}\n".format(time.strftime("%Y%m%d_%H%M%S"),
          os.path.basename(caller.filename), caller.lineno, caller.function, msg))
